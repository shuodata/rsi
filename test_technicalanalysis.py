import unittest
import warnings

from common import generate_prices
from ohlcv import DateOutRange
from technicalanalysis import *

item = {
   "_id": 1600204260000,
   "open": "0.03371800",
   "high": "0.03372700",
   "low": "0.03369200",
   "close": "0.03369500",
   "volume": "413.19300000",
   "close_time": 1600204319999,
   "quote_asset_volume": "13.93363240",
   "number_of_trades": 98
}
prices = generate_prices(item, 70000)


class TechnicalAnalysis_Instantiation(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      warnings.simplefilter('ignore', ResourceWarning)

   def test_TechnicalAnalysis_instantiation(self):
      ta = TechnicalAnalysis(prices)
      self.assertIsInstance(ta, TechnicalAnalysis)


class TechnicalAnalysis_Methods(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      warnings.simplefilter('ignore', ResourceWarning)

   def test_TechnicalAnalysis_filter_DateOutRange1(self):
      ta = TechnicalAnalysis(prices)
      with self.assertRaises(DateOutRange):
         ta.filter(year=2019)  # raise DateOutRange because of data not include 2019


class RSI(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      warnings.simplefilter('ignore', ResourceWarning)

   def test_TechnicalAnalysis_filter1(self):
      ta = TechnicalAnalysis(prices)
      ta.filter(year=2020, month=9, day=16)  # assign filter and check if self.__original_prices is working
      self.assertEqual(len(ta.prices), 1440)
      self.assertEqual(ta.prices.index[0], 1600214400000)
      self.assertEqual(ta.prices.index[-1], 1600300740000)
