## Create a simple Python class, that takes the OHLCV data and calculates the RSI for it.



### Market data generator:

I have create a marked data generator to simulate de market data. Defining just the first point in the serie I can create random points with no constant volatility


```python
from common import generate_prices

item = {
"_id": 1600204260000,
"open": "0.03371800",
"high": "0.03372700",
"low": "0.03369200",
"close": "0.03369500",
"volume": "413.19300000",
"close_time": 1600204319999,
"quote_asset_volume": "13.93363240",
"number_of_trades": 98
}

prices = generate_prices(item, 40000)
```


```python
prices[:3]
```




    [{'_id': 1600204260000,
      'open': '0.03371800',
      'high': '0.03372700',
      'low': '0.03369200',
      'close': '0.03369500',
      'volume': '413.19300000',
      'close_time': 1600204319999,
      'quote_asset_volume': '13.93363240',
      'number_of_trades': 98},
     {'_id': 1600204320000,
      'open': '0.03369500',
      'close': '0.03375785',
      'volume': '470.21554885',
      'close_time': 1600204319999,
      'quote_asset_volume': '12.11210956',
      'number_of_trades': 60,
      'high': '0.03378933',
      'low': '0.03358664'},
     {'_id': 1600204380000,
      'open': '0.03375785',
      'close': '0.03490192',
      'volume': '416.21548639',
      'close_time': 1600204379999,
      'quote_asset_volume': '2.50445812',
      'number_of_trades': 57,
      'high': '0.03490767',
      'low': '0.03465369'}]



### TechnicalAnalysis class instantiation

TechnicalAnalysis class contains method to operate and plot with market data 


```python
from technicalanalysis import TechnicalAnalysis
```


```python
ta = TechnicalAnalysis(prices)
```

### Filter

Filter method define the range in which we want to make calculations and plots


```python
print("Original size:", len(ta))
ta.filter(year=2020, month=9, day=16)
print("New size:", len(ta))
```

    Original size: 40000
    New size: 1440


### Access to data

We always have access to the filter data


```python
ta.prices.sample(10).style
```




<style type="text/css">
</style>
<table id="T_3caa9_">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th class="col_heading level0 col0" >open</th>
      <th class="col_heading level0 col1" >high</th>
      <th class="col_heading level0 col2" >low</th>
      <th class="col_heading level0 col3" >close</th>
      <th class="col_heading level0 col4" >volume</th>
      <th class="col_heading level0 col5" >close_time</th>
      <th class="col_heading level0 col6" >quote_asset_volume</th>
      <th class="col_heading level0 col7" >number_of_trades</th>
    </tr>
    <tr>
      <th class="index_name level0" >_id</th>
      <th class="blank col0" >&nbsp;</th>
      <th class="blank col1" >&nbsp;</th>
      <th class="blank col2" >&nbsp;</th>
      <th class="blank col3" >&nbsp;</th>
      <th class="blank col4" >&nbsp;</th>
      <th class="blank col5" >&nbsp;</th>
      <th class="blank col6" >&nbsp;</th>
      <th class="blank col7" >&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_3caa9_level0_row0" class="row_heading level0 row0" >1600292100000</th>
      <td id="T_3caa9_row0_col0" class="data row0 col0" >0.154517</td>
      <td id="T_3caa9_row0_col1" class="data row0 col1" >0.155687</td>
      <td id="T_3caa9_row0_col2" class="data row0 col2" >0.152980</td>
      <td id="T_3caa9_row0_col3" class="data row0 col3" >0.154322</td>
      <td id="T_3caa9_row0_col4" class="data row0 col4" >18.496210</td>
      <td id="T_3caa9_row0_col5" class="data row0 col5" >1600292099999</td>
      <td id="T_3caa9_row0_col6" class="data row0 col6" >1.888469</td>
      <td id="T_3caa9_row0_col7" class="data row0 col7" >9</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row1" class="row_heading level0 row1" >1600285380000</th>
      <td id="T_3caa9_row1_col0" class="data row1 col0" >0.148998</td>
      <td id="T_3caa9_row1_col1" class="data row1 col1" >0.149336</td>
      <td id="T_3caa9_row1_col2" class="data row1 col2" >0.148059</td>
      <td id="T_3caa9_row1_col3" class="data row1 col3" >0.147735</td>
      <td id="T_3caa9_row1_col4" class="data row1 col4" >359.497423</td>
      <td id="T_3caa9_row1_col5" class="data row1 col5" >1600285379999</td>
      <td id="T_3caa9_row1_col6" class="data row1 col6" >11.301971</td>
      <td id="T_3caa9_row1_col7" class="data row1 col7" >7</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row2" class="row_heading level0 row2" >1600266480000</th>
      <td id="T_3caa9_row2_col0" class="data row2 col0" >0.116567</td>
      <td id="T_3caa9_row2_col1" class="data row2 col1" >0.117549</td>
      <td id="T_3caa9_row2_col2" class="data row2 col2" >0.116593</td>
      <td id="T_3caa9_row2_col3" class="data row2 col3" >0.117013</td>
      <td id="T_3caa9_row2_col4" class="data row2 col4" >338.256942</td>
      <td id="T_3caa9_row2_col5" class="data row2 col5" >1600266479999</td>
      <td id="T_3caa9_row2_col6" class="data row2 col6" >4.409736</td>
      <td id="T_3caa9_row2_col7" class="data row2 col7" >81</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row3" class="row_heading level0 row3" >1600270320000</th>
      <td id="T_3caa9_row3_col0" class="data row3 col0" >0.141887</td>
      <td id="T_3caa9_row3_col1" class="data row3 col1" >0.142785</td>
      <td id="T_3caa9_row3_col2" class="data row3 col2" >0.141854</td>
      <td id="T_3caa9_row3_col3" class="data row3 col3" >0.141680</td>
      <td id="T_3caa9_row3_col4" class="data row3 col4" >478.930017</td>
      <td id="T_3caa9_row3_col5" class="data row3 col5" >1600270319999</td>
      <td id="T_3caa9_row3_col6" class="data row3 col6" >13.723997</td>
      <td id="T_3caa9_row3_col7" class="data row3 col7" >17</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row4" class="row_heading level0 row4" >1600214400000</th>
      <td id="T_3caa9_row4_col0" class="data row4 col0" >0.023330</td>
      <td id="T_3caa9_row4_col1" class="data row4 col1" >0.023459</td>
      <td id="T_3caa9_row4_col2" class="data row4 col2" >0.023223</td>
      <td id="T_3caa9_row4_col3" class="data row4 col3" >0.022765</td>
      <td id="T_3caa9_row4_col4" class="data row4 col4" >31.797327</td>
      <td id="T_3caa9_row4_col5" class="data row4 col5" >1600214399999</td>
      <td id="T_3caa9_row4_col6" class="data row4 col6" >11.935936</td>
      <td id="T_3caa9_row4_col7" class="data row4 col7" >82</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row5" class="row_heading level0 row5" >1600239780000</th>
      <td id="T_3caa9_row5_col0" class="data row5 col0" >0.032868</td>
      <td id="T_3caa9_row5_col1" class="data row5 col1" >0.032940</td>
      <td id="T_3caa9_row5_col2" class="data row5 col2" >0.032645</td>
      <td id="T_3caa9_row5_col3" class="data row5 col3" >0.032855</td>
      <td id="T_3caa9_row5_col4" class="data row5 col4" >271.484917</td>
      <td id="T_3caa9_row5_col5" class="data row5 col5" >1600239779999</td>
      <td id="T_3caa9_row5_col6" class="data row5 col6" >6.904268</td>
      <td id="T_3caa9_row5_col7" class="data row5 col7" >12</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row6" class="row_heading level0 row6" >1600296240000</th>
      <td id="T_3caa9_row6_col0" class="data row6 col0" >0.168640</td>
      <td id="T_3caa9_row6_col1" class="data row6 col1" >0.169007</td>
      <td id="T_3caa9_row6_col2" class="data row6 col2" >0.168183</td>
      <td id="T_3caa9_row6_col3" class="data row6 col3" >0.166654</td>
      <td id="T_3caa9_row6_col4" class="data row6 col4" >20.406851</td>
      <td id="T_3caa9_row6_col5" class="data row6 col5" >1600296239999</td>
      <td id="T_3caa9_row6_col6" class="data row6 col6" >12.077767</td>
      <td id="T_3caa9_row6_col7" class="data row6 col7" >66</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row7" class="row_heading level0 row7" >1600230540000</th>
      <td id="T_3caa9_row7_col0" class="data row7 col0" >0.027431</td>
      <td id="T_3caa9_row7_col1" class="data row7 col1" >0.027534</td>
      <td id="T_3caa9_row7_col2" class="data row7 col2" >0.027344</td>
      <td id="T_3caa9_row7_col3" class="data row7 col3" >0.026614</td>
      <td id="T_3caa9_row7_col4" class="data row7 col4" >108.290463</td>
      <td id="T_3caa9_row7_col5" class="data row7 col5" >1600230539999</td>
      <td id="T_3caa9_row7_col6" class="data row7 col6" >7.677492</td>
      <td id="T_3caa9_row7_col7" class="data row7 col7" >55</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row8" class="row_heading level0 row8" >1600223880000</th>
      <td id="T_3caa9_row8_col0" class="data row8 col0" >0.021676</td>
      <td id="T_3caa9_row8_col1" class="data row8 col1" >0.021800</td>
      <td id="T_3caa9_row8_col2" class="data row8 col2" >0.021605</td>
      <td id="T_3caa9_row8_col3" class="data row8 col3" >0.020952</td>
      <td id="T_3caa9_row8_col4" class="data row8 col4" >236.687900</td>
      <td id="T_3caa9_row8_col5" class="data row8 col5" >1600223879999</td>
      <td id="T_3caa9_row8_col6" class="data row8 col6" >11.386775</td>
      <td id="T_3caa9_row8_col7" class="data row8 col7" >56</td>
    </tr>
    <tr>
      <th id="T_3caa9_level0_row9" class="row_heading level0 row9" >1600262100000</th>
      <td id="T_3caa9_row9_col0" class="data row9 col0" >0.100936</td>
      <td id="T_3caa9_row9_col1" class="data row9 col1" >0.101109</td>
      <td id="T_3caa9_row9_col2" class="data row9 col2" >0.100878</td>
      <td id="T_3caa9_row9_col3" class="data row9 col3" >0.100302</td>
      <td id="T_3caa9_row9_col4" class="data row9 col4" >242.752856</td>
      <td id="T_3caa9_row9_col5" class="data row9 col5" >1600262099999</td>
      <td id="T_3caa9_row9_col6" class="data row9 col6" >11.677126</td>
      <td id="T_3caa9_row9_col7" class="data row9 col7" >60</td>
    </tr>
  </tbody>
</table>




### Plot market data

Class allows to Plot the market data filtered


```python
ta.Plot(title="ETH-BTC")
```


    <Figure size 432x288 with 0 Axes>



    
![png](output_12_1.png)
    


### Calculate RSI 

We can calculate the RSI based on the filter


```python
ta.RSI(nonan=True)
```




    _id
    1600214400000    69.434267
    1600214460000    69.434267
    1600214520000    69.434267
    1600214580000    85.723586
    1600214640000    32.898555
                       ...    
    1600300500000    40.399011
    1600300560000    42.489844
    1600300620000    43.004512
    1600300680000    48.843471
    1600300740000    46.649835
    Name: RSI, Length: 1440, dtype: float64



### Plot RSI and volumen

We can plot the rsi values and the volume. I represent value over 70 and under 30 with different colors on rsi and volumes values


```python
ta.RSIPlot()
```


    <Figure size 432x288 with 0 Axes>



    
![png](output_16_1.png)
    

