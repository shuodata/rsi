from pandas import DataFrame, to_datetime
import matplotlib.pyplot as plt
import calendar


class DataInWrongFormat(Exception):
   pass


class WrongDateRange(Exception):
   pass


class DateOutRange(Exception):
   pass


'''
   This class is used to store the OHLCV data in a pandas DataFrame. 
   The data is stored and then converted to a DataFrame.
   The DataFrame is then stored in a variable called prices.
   '''
class OHLCV(object):

   def __init__(self, ohlcv) -> None:
      if isinstance(ohlcv, DataFrame):
         self.__prices = ohlcv.price.copy()
      elif isinstance(ohlcv, list):
         self.prices = ohlcv
      else:
         raise DataInWrongFormat("OHLCV.__init__: Data is not in the correct format (list or DataFrame)")
      self.__original_prices = self.__prices.copy()

   @property
   def prices(self) -> DataFrame:
      return self.__prices

   @prices.setter
   def prices(self, value: list) -> None:
      try:
         __temp = DataFrame(value)
         __temp[["open", "close", "high", "low", "volume", "quote_asset_volume"]] = __temp[
            ["open", "close", "high", "low", "volume", "quote_asset_volume"]].astype(float)
         self.__prices = __temp.set_index('_id')
      except Exception as e:
         raise DataInWrongFormat(f"Some column is not in proper format: {e}")

   def __str__(self) -> str:
      return self.prices.to_string()

   def __repr__(self) -> str:
      return self.__str__()

   def __dir__(self) -> list:
      return self.prices.columns.to_list()

   def __getitem__(self, item) -> dict:
      return self.prices.loc[item].to_dict()

   def __len__(self) -> int:
      return len(self.prices)

   def __iter__(self) -> iter:
      return self.prices.iterrows()

   def __contains__(self, item) -> bool:
      return item in self.prices.index

   def __eq__(self, other) -> bool:
      return self.prices.equals(other.prices)

   def __ne__(self, other) -> bool:
      return not self.__eq__(other)

   def __copy__(self):
      return OHLCV(self.prices.copy())

   def filter(self, day: int = None, month: int = None, year: int = None):

      start = None
      end = None
      if year is not None:
         start = f"{year}-01-01 00:00:00"
         end = f"{year}-12-31 23:59:59"
      if month is not None:
         if year is None:
            raise WrongDateRange("OHLCV.filter: Year is not specified")
         start = f"{year}-{month}-01 00:00:00"
         end = f"{year}-{month}-{calendar.monthrange(year, month)[1]} 23:59:59"

      if day is not None:
         if year is None or month is None:
            raise WrongDateRange("OHLCV.filter: Year or month is not specified")
         start = f"{year}-{month}-{day} 00:00:00"
         end = f"{year}-{month}-{day} 23:59:59"

      # convert start to timestamp
      start = int(to_datetime(start).timestamp() * 1000)
      end = int(to_datetime(end).timestamp() * 1000)

      if start > self.__original_prices.index.max():
         # print("start", self.prices.index.min() , start)
         raise DateOutRange("OHLCV.filter: Start date is out of range")
      if end < self.__original_prices.index.min():
         # print("end", self.prices.index.max(), end)
         raise DateOutRange("OHLCV.filter: End date is out of range")

      self.__prices = self.__original_prices[
         (self.__original_prices.index >= start) & (self.__original_prices.index <= end)]

   def Plot(self, start: int = None, end: int = None, width: int = 50000, title: str = None) -> None:
      prices = self.prices.copy()
      prices = prices.loc[start:] if start is not None and start in prices.index else prices
      prices = prices.loc[:end] if end is not None and end in prices.index else prices

      plt.clf()
      plt.figure(figsize=(16, 9))
      width2 = width / 10

      up = prices[prices.close >= prices.open]
      down = prices[prices.close < prices.open]

      plt.bar(up.index, up.close - up.open, width, bottom=up.open, color='green')
      plt.bar(up.index, up.high - up.close, width2, bottom=up.close, color='green')
      plt.bar(up.index, up.low - up.open, width2, bottom=up.open, color='green')

      plt.bar(down.index, down.close - down.open, width, bottom=down.open, color='red')
      plt.bar(down.index, down.high - down.open, width2, bottom=down.open, color='red')
      plt.bar(down.index, down.low - down.close, width2, bottom=down.close, color='red')

      plt.xticks(rotation=45, ha='right')
      plt.ylim([prices.low.min() * 0.97, prices.high.max() * 1.01])
      if title is not None:
         plt.title(title)

      plt.show()
