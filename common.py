import numpy as np



def generate_prices(first_value, size):
   def randomwalk(currentprice):
      if isinstance(currentprice, str):
         currentprice = float(currentprice)
      __min_volatility = currentprice * 0.01
      __max_volatility = __min_volatility * 3

      return str(
         round(np.random.normal(loc=currentprice, scale=np.random.uniform(low=__min_volatility, high=__max_volatility)),
               8))

   result = [first_value]
   nextitem = first_value
   for i in range(size-1):
      nextitem = {
         "_id": nextitem["_id"] + 60000,
         "open": nextitem["close"],
         "close": randomwalk(nextitem["close"]),
         "volume": str(round(np.random.uniform(low=1, high=500), 8)),
         "close_time": nextitem["_id"] + 59999,
         "quote_asset_volume": str(round(np.random.uniform(low=1, high=14), 8)),
         "number_of_trades": int(np.random.uniform(low=1, high=100))
      }
      nextitem["high"] = str(
         round(max(float(nextitem["open"]), float(nextitem["close"])) * (1 + np.random.uniform(low=0, high=0.01)), 8))
      nextitem["low"] = str(
         round(max(float(nextitem["open"]), float(nextitem["close"])) * (1 - np.random.uniform(low=0, high=0.01)), 8))
      result.append(nextitem)
   return result
