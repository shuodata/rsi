import unittest
import warnings

from common import generate_prices
from ohlcv import OHLCV, WrongDateRange, DateOutRange

item = {
   "_id": 1600204260000,
   "open": "0.03371800",
   "high": "0.03372700",
   "low": "0.03369200",
   "close": "0.03369500",
   "volume": "413.19300000",
   "close_time": 1600204319999,
   "quote_asset_volume": "13.93363240",
   "number_of_trades": 98
}
prices = generate_prices(item, 70000)


class OHLCV_Instantiation(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      warnings.simplefilter('ignore', ResourceWarning)

   def test_OHLCV_instantiation(self):
      ohlcv = OHLCV(prices)
      self.assertIsInstance(ohlcv, OHLCV)


class OHLCV_Methods(unittest.TestCase):
   @classmethod
   def setUpClass(cls):
      warnings.simplefilter('ignore', ResourceWarning)

   def test_OHLCV_filter_DateOutRange1(self):
      ohlcv = OHLCV(prices)
      with self.assertRaises(DateOutRange):
         ohlcv.filter(year=2019)  # raise DateOutRange because of data not include 2019

   def test_OHLCV_filter_DateOutRange2(self):
      ohlcv = OHLCV(prices)
      with self.assertRaises(DateOutRange):
         ohlcv.filter(year=2020, month=1)  # raise DateOutRange because of data not include January 2020

   def test_OHLCV_filter_DateOutRange3(self):
      ohlcv = OHLCV(prices)
      with self.assertRaises(DateOutRange):
         ohlcv.filter(year=2020, month=1, day=1)  # raise DateOutRange because of data not include 1st January 2020

   def test_OHLCV_filter_WrongDateRange1(self):
      ohlcv = OHLCV(prices)
      with self.assertRaises(WrongDateRange):
         ohlcv.filter(month=1, day=1)  # raise WrongDateRange because year is not defined

   def test_OHLCV_filter_WrongDateRange2(self):
      ohlcv = OHLCV(prices)
      with self.assertRaises(WrongDateRange):
         ohlcv.filter(day=1)  # raise WrongDateRange because year and month are not defined

   def test_OHLCV_filter1(self):
      ohlcv = OHLCV(prices)
      ohlcv.filter(year=2020, month=9, day=16)  # assign filter and check if self.__original_prices is working
      self.assertEqual(len(ohlcv.prices), 1440)
      self.assertEqual(ohlcv.prices.index[0], 1600214400000)
      self.assertEqual(ohlcv.prices.index[-1], 1600300740000)

   def test_OHLCV_filter2(self):
      ohlcv = OHLCV(prices)
      ohlcv.filter(year=2020, month=10)  # assign filter and check if self.__original_prices is working
      self.assertEqual(len(ohlcv.prices), 44640)
      self.assertEqual(ohlcv.prices.index[0], 1601510400000)
      self.assertEqual(ohlcv.prices.index[-1], 1604188740000)

   def test_OHLCV_filter3(self):
      ohlcv = OHLCV(prices)
      ohlcv.filter(year=2020, month=10)  # assign filter
      ohlcv.filter(year=2020, month=9)  # reassign filter and check if self.__original_prices is working
      self.assertEqual(len(ohlcv.prices), 21769)
      self.assertEqual(ohlcv.prices.index[0], 1600204260000)
      self.assertEqual(ohlcv.prices.index[-1], 1601510340000)
